//
//  SecondViewController.swift
//  WebviewCommu
//
//  Created by Korakod Saraboon on 24/4/2567 BE.
//

import UIKit
import UserNotifications

class SecondViewController: UIViewController {

  @IBOutlet weak var keepAliveLabel: UILabel!
  
  @IBOutlet weak var imageView: UIImageView!
  var base64String: String?
  
  override func viewDidLoad() {
        super.viewDidLoad()
    imageView.image = convertBase64StringToImage(imageBase64String: base64String ?? "")
        // Do any additional setup after loading the view.
    }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(_:)), name: NSNotification.Name(rawValue: "MyNotification"), object: nil)
    navigationController?.isNavigationBarHidden = false
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    navigationController?.isNavigationBarHidden = true
    NotificationCenter.default.removeObserver(self)
  }

  
  @objc func handleNotification(_ notification: Notification) {
         if let userInfo = notification.userInfo {
             if let message = userInfo["message"] as? String {
               keepAliveLabel.text = message
             }
         }
     }
  
  func convertBase64StringToImage (imageBase64String:String) -> UIImage {
      let imageData = Data(base64Encoded: imageBase64String)
      let image = UIImage(data: imageData!)
      return image!
  }
}
