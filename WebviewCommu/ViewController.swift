//
//  ViewController.swift
//  WebviewCommu
//
//  Created by Korakod Saraboon on 2/11/2566 BE.
//

import UIKit
import WebKit

class ViewController: UIViewController {
  
  @IBOutlet weak var webview: WKWebView!
  @IBOutlet weak var urltextbox: UITextField!
  @IBOutlet weak var messageTextView: UITextView!
  
  let defaultURL = "https://rounded-famous-marsupial.glitch.me/"
  let handler = "CRSanswerfatca"
  let handler2 = "keepAlivefatca"
  
  var timer: Timer?
  var timeInt: Int = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    setupWebview()
  }
  
  func setupWebview() {
    urltextbox.text = defaultURL
  }
  
  func openWebView() {
    let link = URL(string: urltextbox.text ?? defaultURL)!
    let request = URLRequest(url: link)
    webview.load(request)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    //clearJsBridge()
  }
  
  deinit {
    clearJsBridge()
  }
  
  func clearJsBridge() {
    webview.configuration.userContentController.removeScriptMessageHandler(forName: "keepAlivefatca")
    webview.configuration.userContentController.removeScriptMessageHandler(forName: "CRSanswerfatca")
  }
  
  func sendMessage() {
    let mess = messageTextView.text ?? ""
    webview.evaluateJavaScript("ReceivedMessage(\"\(mess)\")")
  }
  
  @IBAction func renderWebview(_ sender: Any) {
    openWebView()
  }
  
  @IBAction func sendMessage(_ sender: Any) {
    sendMessage()
  }
  
  @IBAction func startAlive(_ sender: Any) {
    keepAlive()
  }
  
  @IBAction func stopAlive(_ sender: Any) {
    timer?.invalidate()
    timeInt = 0
  }
  
  @IBAction func nextScreen(_ sender: Any) {
    let vc = SecondViewController(nibName: "SecondViewController", bundle: nil)
    vc.base64String = messageTextView.text
    navigationController?.pushViewController(vc, animated: true)
  }
  
  @IBAction func clearJS(_ sender: Any) {
    clearJsBridge()
  }
  
  @IBAction func regis(_ sender: Any) {
    webview.configuration.userContentController.add(self, name: "CRSanswerfatca")
    webview.configuration.userContentController.add(self, name: "keepAlivefatca")
  }
  
  func keepAlive() {
    timer?.invalidate()
    timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
  }
  
  @objc func updateTimer() {
    // update keep alive to website
    timeInt += 1
    webview.evaluateJavaScript("ReceivedKeepAliveMessage(\"\(timeInt)\")")
  }
  
  func showAlert() {
    let dialogMessage = UIAlertController(title: "Failed", message: "Webview tell us something failed", preferredStyle: .alert)
    dialogMessage.addAction(UIAlertAction(title: "OK", style: .default))
    self.present(dialogMessage, animated: true, completion: nil)
  }
}

extension ViewController: WKScriptMessageHandler, WKNavigationDelegate {
  
  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    let result = message.body as? String
    let channel = message.name as? String
    switch (channel) {
    case "CRSanswerfatca":
      messageTextView.text = result
    case "keepAlivefatca":
      let userInfo = ["message": result]
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MyNotification"), object: nil, userInfo: userInfo)
      print(result)
    default:
      print("No Case")
    }
  }
  
  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    // start to load webview
    // show loading or something
  }
  
  func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
    // show error popup
    // show retry button or back
  }
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    // stop loading
    // ready to engage
  }
  
  func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
    // logic to check anysite is reachable or allow domain
    decisionHandler(.allow)
  }
  
}


